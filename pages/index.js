import { useEffect, useState } from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useInView } from "react-intersection-observer";
// import { useRouter } from "next/router";

import { Header } from "../components/header";
import { Navigation } from "../components/navigation";
import { Footer } from "../components/footer";

import styles from "../styles/Home.module.scss";

const lists = [
  {
    key: "#issue5",
    imageURL: "backstagetalks_cover_issue_5.png",
    title: "Issue #5",
    isSoldOut: false,
    color: "#00c1b5",
  },
  {
    key: "#issue4",
    imageURL: "backstagetalks_cover_issue_4.png",
    title: "Issue #4",
    isSoldOut: false,
    color: "#ff651a",
  },
  {
    key: "#issue3",
    imageURL: "backstagetalks_cover_issue_3.png",
    title: "Issue #3",
    isSoldOut: false,
    color: "#ffbe00",
  },
  {
    key: "#issue2",
    imageURL: "backstagetalks_cover2017.png",
    title: "Issue #2",
    isSoldOut: false,
    color: "#1d3fbb",
  },
  {
    key: "#issue1",
    imageURL: "backstagetalks_cover2016_n.png",
    title: "Issue #1",
    isSoldOut: true,
    color: "#e30512",
  },
];

const SectionWrapper = ({
  id,
  imageURL,
  title,
  isSoldOut,
  color,
  setCurrentIssue,
}) => {
  const {
    ref: sectionRef,
    inView,
    entry,
  } = useInView({
    rootMargin: "0% 0% 100% 0%",
    // delay: 100,
    // trackVisibility: true,
    threshold: 1,
  });

  useEffect(() => {
    if (inView) {
      setCurrentIssue({ key: id, imageURL, title, isSoldOut, color });
    }
  }, [inView]);

  return (
    <section ref={sectionRef} id={id} className={styles["section"]}>
      <div className={styles["section__content"]}>
        <Image src={`/images/${imageURL}`} width="420" height="537" />
        <h4 className={styles["section__title"]}>
          {title} {isSoldOut ? "is sold out." : null}
        </h4>
        {!isSoldOut && (
          <h5 className={styles["buy-now"]}>
            <Link href="/">
              <a>Buy Here</a>
            </Link>
          </h5>
        )}
        <h5>
          {isSoldOut
            ? "If you are lucky, you may get the last pieces in"
            : "or in"}{" "}
          <span>
            <Link href="/">
              <a>selected stores</a>
            </Link>
          </span>
        </h5>
      </div>
    </section>
  );
};

export default function Home() {
  // const router = useRouter();
  const [currentIssue, setCurrentIssue] = useState(lists[0] ?? null);

  // useEffect(() => {
  //   if (currentIssue) {
  //     router.push(`/${currentIssue.key}`);
  //   }
  // }, [currentIssue]);
  return (
    <div className={styles.container}>
      <Head>
        <title>Backstage Talks Magazine</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Navigation links={lists} currentLink={currentIssue} />

      <main
        className={styles["main"]}
        // style={{ background: "#ffbe00" }}
        style={{ background: currentIssue.color }}
        onWheel={(e) => {
          // if (e.deltaY > 0) {
          //   console.log("down");
          // } else if (e.deltaY < 0) {
          //   console.log(lists[])
          //   console.log("up");
          // }
        }}
      >
        {lists.map(({ key, ...rest }) => {
          return (
            <SectionWrapper
              key={key}
              id={key}
              setCurrentIssue={setCurrentIssue}
              {...rest}
            />
          );
        })}
      </main>
      <Footer />
    </div>
  );
}
