import Link from "next/link";
import styles from "../styles/footer.module.scss";

export const Footer = () => {
  return (
    <footer className={styles["main"]}>
      <div className={styles["main__content"]}>
        <h4 className={styles["content__text"]}>
          Backstage Talks is a magazine of casual, but in depth dialogues on
          design and business. Our decisions shape and influence this complex
          world—to have a chance to make the right ones, we need to talk.
        </h4>
        <p className={styles["copyright"]}>
          © 2021{" "}
          <span>
            <Link href="/">
              <a>Published by studio Milk</a>
            </Link>
          </span>
        </p>
      </div>

      <div className={styles["legal"]}>
        <p>
          <Link href="/">
            <a>Privacy Policy</a>
          </Link>
        </p>
      </div>
    </footer>
  );
};
