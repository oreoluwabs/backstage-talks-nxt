import Image from "next/image";
import Link from "next/link";

import styles from "../styles/header.module.scss";

export const Header = () => {
  return (
    <header className={styles["main"]}>
      <nav className={styles["nav"]}>
        <Image src="/images/logo.png" width="260px" height="30px" />

        <div>
          <ul>
            <li>
              <Link href="/">
                <a>info@backstagetalks.com</a>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};
