import Link from "next/link";

export const Navigation = ({ links, currentLink }) => {
  return (
    <div
      style={{
        position: "fixed",
        right: 0,
        bottom: 0,
        padding: "1.1rem 0.9rem",
        zIndex: 9,
      }}
    >
      <ul style={{ listStyleType: "none" }}>
        {links.map(({ title, key }) => (
          <li key={key}>
            <Link href={`/`}>
              <a
                style={{
                  fontWeight: currentLink.key == key ? 700 : "normal",
                }}
              >
                {title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};
